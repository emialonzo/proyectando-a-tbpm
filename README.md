# Proyecto de generador

## Instrucciones

### Dependencias
 - nodej https://nodejs.org/
 - JRE http://openjdk.java.net/
 
### Instalación
```
git clone git@gitlab.fing.edu.uy:emiliano.alonzo/proyectando-a-tbpm.git
cd proyectando-a-tbpm/app
sh instalar.sh 
```

### Utilización
```
npm start
```

## Autores
    Nombre: 	Emiliano Alonzo Fulchi
    E-mail: 	emialonzo@gmail.com
    Teléfono: 	098 225 975

    Nombre: 	Rodrigo Sebastian Damiano Izzi
    E-mail: 	rodrigodam08@gmail.com
    Teléfono: 	099 333 225

## Tutores
  * Nombre: 	Andrea Delgado
    E-mail: 	adelgado@fing.edu.uy

  * Nombre: 	Daniel Calegari
    E-mail: 	dcalegar@fing.edu.uy


## Resumen
El modelado en el proceso de desarrollo de software es realizado en general como documentación del sistema que difícilmente es mantenida luego de implementado y liberado el mismo. Por el contrario, el Desarrollo Dirigido por Modelos (Model Driven Development, MDD) basa el desarrollo de software en modelos, utilizando como artefactos de primer orden metamodelos, modelos y lenguajes que permiten transformaciones entre éstos. Las transformaciones pueden ser horizontales al mismo nivel de abstracción entre modelos o artefactos equivalentes semánticamente, o verticales refinando el nivel de abstracción mediante transformaciones sucesivas de un modelo en otro modelo del mismo sistema hasta llegar al código asociado.

Un elemento clave para el desarrollo de sistemas que cumplan con las expectativas y planteos de los clientes es la captura de requerimientos, los artefactos generados para comunicación con el cliente en dicha etapa, y los acuerdos alcanzados en base al entendimiento común de los mismos. Existen diversas propuestas para agilizar esta etapa y alcanzar acuerdos que sirvan de base para el desarrollo a realizar, como por ejemplo la realización temprana de prototipos evolutivos, esto es prototipos no desechables sino integrables en el sistema final. Se han utilizado distintos artefactos de comunicación con los clientes, como los casos de uso, que han aportado a lograr estos acuerdos pero sin alcanzar la aceptación esperada.

Un modelo de procesos de negocio expresado en una notación como BPMN 2.0 provee las bases para acordar en elementos básicos de los sistemas como el flujo de secuencia entre las actividades, los tipos de actividades a realizar (de usuario con formularios, automáticas invocando web services, de reglas de negocio ejecutando políticas de la organización, entre otras), los caminos posibles entre actividades (con nodos de decisión), y presentar rápidamente al usuario con las pantallas y tipos de ejecución posibles según el modelo.

Como elemento principal de comunicación con el cliente se quiere utilizar una notación textual adecuada (a definir en base a lenguajes y propuestas existentes) para generar modelos de procesos de negocio desde dicha notación textual. Se evaluarán propuestas existentes (incluyendo un lenguaje de base que se está definiendo en el contexto del cliente) y se definirán los elementos necesarios para poder expresar no sólo el flujo de trabajo del proceso, sino además los usuarios que deben realizar las tareas y los datos que están involucrados en su realización.

El objetivo de este proyecto es especificar un lenguaje para descripción de procesos de negocio en forma textual y transformaciones para la generación automática de los modelos y perspectivas asociadas (flujo de control, recursos, datos) desde dicha descripción, incluyendo un prototipo funcional de la propuesta.

## Resultado esperados

* [x] Relevamiento de estrategias de generación de modelos de procesos de negocio desde texto

* [x] Selección y adaptación de las propuestas para la representación del flujo de control, recursos y datos

* [x] Especificación de lenguaje para descripción de procesos de negocio en forma textual y transformaciones para la generación automática de los modelos y perspectivas asociadas (flujo de control, recursos, datos)

* [x] Prototipo funcional para la generación automática de modelos de procesos de negocio desde texto

* [x] Caso de estudio de aplicación de la propuesta en un dominio específico

* [x] Informe del proyecto de grado final y durante todo el proyecto por capítulos según etapas

